require 'test_helper'

class CommentsControllerTest < ActionController::TestCase
  setup do
    @comment = comments(:one)
  end

  test "should get index" do
	  assert_raises(ActionController::UrlGenerationError) do
        get :index
	  end
  end

  test "should get new" do
	  assert_raises(ActionController::UrlGenerationError) do
		  get :new
	  end
  end

  test "should create comment" do

	  assert_difference('Comment.count') do
		  post :create, comment: { body: @comment.body, post_id: @comment.post_id }, post_id: @comment.post_id
	  end
	  assert_redirected_to comment_path(assigns(:comment))
  end

  test "should show comment" do
	assert_raises(ActionController::UrlGenerationError) do
	  get :show, id: @comment
    end
  end

  test "should get edit" do
	  assert_raises(ActionController::UrlGenerationError) do
		  get :edit, id: @comment
	  end
  end

  test "should update comment" do
	  assert_raises(ActionController::UrlGenerationError) do
		  patch :update, id: @comment, comment: { body: @comment.body, post_id: @comment.post_id }
	  end
  end

  test "should destroy comment" do
    assert_raises(ActionController::UrlGenerationError) do
      delete :destroy, id: @comment
    end
  end
end
